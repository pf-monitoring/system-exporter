#!/usr/bin/env python

import configparser
import os
import platform
from prometheus_client import start_http_server, Gauge, REGISTRY
from pystemd.systemd1 import Unit
import signal
import time

for i in list(REGISTRY._collector_to_names.keys()):
    REGISTRY.unregister(i)

DEFAULT_POLL_INTERVAL = 10
DEFAULT_HTTP_PORT = 64903

def poll_system_systemd_units(system_systemd_units):
    for unit in system_systemd_units:
        unit_object = Unit(unit["name"], _autoload = True)
        if unit_object.Unit.SubState.decode() == "running":
            unit["alive"].set(1)
        else:
            unit["alive"].set(0)

def poll_system_uptime(system_uptime):
    uptime = 0
    try:
        f = open("/proc/uptime", "r")
        uptime = float(f.readline().split()[0])
        f.close()
    except (IOError, ValueError):
        pass
    system_uptime.set(uptime)

def sigterm_handler(_signo, _stack_frame):
    exit(os.EX_OK)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, sigterm_handler)
    signal.signal(signal.SIGTERM, sigterm_handler)

    config_daemon = configparser.ConfigParser()
    config_daemon.read("/etc/system-exporter/daemon.conf")
    hostname = config_daemon.get("daemon", "hostname", fallback = platform.node())
    if not hostname:
        hostname = platform.node()
    escaped_hostname = hostname.replace(".", "_")
    poll_interval = int(config_daemon.get("daemon", "poll-interval", fallback = DEFAULT_POLL_INTERVAL))
    http_port = int(config_daemon.get("daemon", "http-port", fallback = DEFAULT_HTTP_PORT))

    system_systemd_units = []
    if "systemd" in config_daemon:
        if "units" in config_daemon["systemd"]:
            for unit in config_daemon["systemd"]["units"].split(","):
                if unit:
                    escaped_unit = unit
                    for i in ".-@":
                        escaped_unit = escaped_unit.replace(i, "_")
                    system_systemd_units.append({
                        "name": unit,
                        "alive": Gauge("system_systemd_unit_{}_{}".format(escaped_hostname, escaped_unit), "If {} on {} is alive".format(unit, hostname)),
                        })

    system_uptime = None
    if "uptime" in config_daemon:
        system_uptime = Gauge("system_uptime_{}".format(escaped_hostname), "{} uptime".format(hostname))

    ready = False
    while True:
        if system_systemd_units:
            poll_system_systemd_units(system_systemd_units)
        if system_uptime:
            poll_system_uptime(system_uptime)
        if not ready:
            ready = True
            start_http_server(http_port)
        time.sleep(poll_interval)
